﻿#include <iostream>
#include <cstring>
using namespace std;

// число вершин в графе
#define V 5

int G[V][V] = 
{
    {0, 5, 5, 2, 0},
    {5, 0, 2, 3, 2 },
    {5, 2, 0, 0, 2},
    {2, 3, 0, 0, 0},
    {0, 2, 2, 0, 0},
};

int main() 
{
    setlocale(LC_ALL, "RUS");

    int no_edge = 0;            // число ребер

    //массив для отслеживания выбранной вершины
    int selected[V];

    memset(selected, false, sizeof(selected)); //присвоение начальных значений определенной обл памяти

    selected[0] = true;

    int x;            //  номер строки
    int y;            //  столбца

    cout << "Ребро и его вес" << endl;
    while (no_edge < V - 1) 
    {

        //Для каждой вершины в множестве S найдите все смежные вершины
         // , вычислите расстояние от вершины , выбранной на шаге 1.
         // если вершина уже находится в наборе S, отбросьте ее в противном случае
         //выберите другую вершину, ближайшую к выбранной вершине на шаге 1.

        int min = 999999999;
        x = 0;
        y = 0;

        for (int i = 0; i < V; i++) 
        {
            if (selected[i]) 
            {
                for (int j = 0; j < V; j++) 
                {
                    if (!selected[j] && G[i][j] && min > G[i][j])
                    { // не в selected и на диагонали и наименьшая

                            min = G[i][j];
                            x = i;
                            y = j;
                        

                    }
                }
            }
        }
        cout << x << "-" << y << " = " << G[x][y] << endl;
        selected[y] = true;
        no_edge++;
    }

    return 0;
}