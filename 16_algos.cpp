﻿#include <iostream>
//уложить как можно большее число ценных вещей в рюкзак при условии, что вместимость рюкзака ограничена. 
using namespace std;

int main()
{
    setlocale(LC_CTYPE, "rus");
    const int kolvo_predmetov = 5;
    double W = 20;
    double w[kolvo_predmetov] = {10, 4, 17, 20, 1}; // масса
    double price[kolvo_predmetov] = {12, 11, 17, 15, 6}; // стоимость
    double mass[10][kolvo_predmetov]; //массив для расчетов
    double weight = 0; //суммарный вес
    double sum_sloimost = 0; // суммарная стоимость

    for (int i = 0; i < kolvo_predmetov; i++)  //подсчет коэффициентов и заполнение строк массива
    {
        mass[0][i] = (price[i] / w[i]);
        mass[1][i] = w[i];
        mass[2][i] = price[i];
        mass[3][i] = i + 1;

    }
    for (int i = 0; i < kolvo_predmetov; i++) //распологаем в порядке убывания
    {
        for (int j = 0; j < kolvo_predmetov; j++)
        {
            double tmp;
            if (mass[0][i] > mass[0][j])
            {
                for (int z = 0; z < 4; z++)
                {
                    tmp = mass[z][i];
                    mass[z][i] = mass[z][j];
                    mass[z][j] = tmp;
                }

            }
        }
    }
    cout << "Подходящие предметы: ";
    for (int i = 0; i < kolvo_predmetov; i++)
    {
        weight += mass[1][i];
        if (weight <= W)
        {
            cout << mass[3][i] << " ";
            sum_sloimost += mass[2][i];
        }
        else
        {
            weight -= mass[1][i];
        }
    }
    cout << endl << "Общий вес: " << weight << endl << "Сумма: " << sum_sloimost << endl << endl;
    return 0;
}