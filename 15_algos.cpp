﻿#include <iostream>
#include <vector>
#include <fstream>

std::vector<int> result;
//раскрашивать так, чтобы соседние вершины имели разные цвета
void GraphColoring(const int v, std::vector<std::vector<int>>& adj)
{
    result.push_back(0);

    // В начале все вершины не окрашены, кроме первой вершины
    for (int u = 1; u < v; u++)
        result.push_back(-1);

    // вектор указывает какой цвет доступен для конкретной вершины
    bool* available = new bool[v];
    for (int color = 0; color < v; color++)
        available[color] = false;

    //true для available[color] значат что цвет color присвоен одной из его соседних вершин
    for (int u = 1; u < v; u++)
    {
        for (int i = 0; i < v; i++) 
        {
            if (adj[i][u] == 1)
                if (result[i] != -1)
                    available[result[i]] = true;
        }

        int color;
        for (color = 0; color < v; color++)
            if (available[color] == false)
                break;

        result[u] = color;

        // сброс значения доступно для следующей итерации
        for (int i = 0; i < v; i++) 
        {
            if (adj[i][u] == 1)
                if (result[i] != -1)
                    available[result[i]] = false;
        }
    }
}

int main() 
{
    setlocale (LC_ALL, "RUS");
    int v, e;
    std::ifstream fin1("input1.txt");
    fin1 >> v >> e;
    std::vector<std::vector<int>> adj(v, std::vector<int>(v, 0));
    for (int i = 0; i < e; i++) 
    {
        int a, b; 
        fin1 >> a >> b;
        adj[a][b] = 1;
        adj[b][a] = 1;
    }
    GraphColoring(v, adj);

    for (int u = 0; u < v; u++)
        std::cout << "Вершина " << u << " получает цвет под номером: " << result[u]+1 << std::endl;
    return 0;
}