﻿#include <iostream>
#include <vector>
#include <queue>
#include <fstream>

std::vector<int> edges[100];
int number_vertices; 
int number_edges; 
bool used[100];
std::vector<int> comp;

void read_graph()  //расставляем ребра   ЗАПИШИ ВЕРШИНЫ И РЕБРА ДЛЯ СВОЕГО ГРАФА!
{
    int v, u;

    std::ifstream fin1("input1.txt");
    fin1 >> number_edges >> number_vertices;
    for (int i = 0; i < number_edges; i++)
    {
        fin1 >> v >> u;
        edges[v].push_back(u);
        edges[u].push_back(v);
    }
}

void BFS(int u) //обход в ширину
{
    used[u] = true;
    std::queue<int> q;
    q.push(u);
    comp.push_back(u);
    while (!q.empty()) 
    {
        int u = q.front();
        q.pop();
        for (int i = 0; i < edges[u].size(); i++) 
        {
            int v = edges[u][i];
            if (!used[v]) 
            {
                used[v] = true;
                comp.push_back(v);
                q.push(v);
            }
        }
    }
}

void DFS(int v) //обход в глубину
{
    used[v] = true;
    comp.push_back(v);
    for (int i = 0; i < edges[v].size(); ++i) 
    {
        int t = edges[v][i];
        if (!used[t])
            DFS(t);
    }
}

void comps_DFS() 
{
    int kolvo = 0;
    for (int i = 0; i < number_vertices; ++i) 
        used[i] = false;
    for (int j = 0; j < number_vertices; ++j) 
        if (!used[j]) 
        {
            comp.clear();
            DFS(j);
            kolvo++;
            std::cout << "Компонент: ";
            for (int k = 0; k < comp.size(); ++k)
                std::cout << ' ' << comp[k];
            std::cout << std::endl;
        }
    std::cout << "кол во компонентов " << kolvo;
    std::cout << std::endl;
}

void comps_BFS() 
{
    int kolvo = 0;
    for (int i = 0; i < number_vertices; ++i)
        used[i] = false;
    for (int j = 0; j < number_vertices; ++j)
        if (!used[j]) 
        {
            comp.clear();
            BFS(j);
            kolvo++;
            std::cout << "Компонент: ";
            for (int k = 0; k < comp.size(); ++k)
                std::cout << ' ' << comp[k];
            std::cout << std::endl;
        }
    std::cout << "кол во компонентов " << kolvo;
    std::cout << std::endl;
}

int main()
{
    setlocale(LC_ALL, "RUS");
    read_graph();
    std::cout << "Обход по DFS" << std::endl;
    comps_DFS();
    std::cout << "Обход по BFS" << std::endl;
    comps_BFS();
}