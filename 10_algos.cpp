﻿#include <iostream>
#include <stdlib.h>
//5 вершин
//0 1 1 1 1    
//1 0 1 1 1
//1 1 0 1 1
//1 1 1 0 1
//1 1 1 1 0
//для стека
struct Node
{
	int inf;
	Node* next;
};

void push(Node*& st, int dat)
{  
	Node* el = new Node;
	el->inf = dat;
	el->next = st;
	st = el;
}

int pop(Node*& st)
{    
	int value = st->inf;
	Node* temp = st;
	st = st->next;
	delete temp;

	return value;
}

int peek(Node* st)
{     // достаем для просмотра число
	return st->inf;
}

//для графа
Node** graph;  
const int vertex = 1; // Первая вершина

void add(Node*& list, int data)
{  //Добавление смежной вершины

	if (!list) 
	{	
		list = new Node; 
		list->inf = data; 
		list->next = 0; 
		return; 
	}

	Node* temp = list;
	while (temp->next)  
		temp = temp->next;
	Node* elem = new Node;
	elem->inf = data;
	elem->next = NULL;
	temp->next = elem;
}

void del(Node*& l, int key)
{ 
	if (l->inf == key) 
	{
		Node* tmp = l; 
		l = l->next; 
		delete tmp; 
	}
	else
	{
		Node* tmp = l;
		while (tmp)
		{
			if (tmp->next) // есть следующая вершина
				if (tmp->next->inf == key)
				{  // и она искомая
					Node* tmp2 = tmp->next;
					tmp->next = tmp->next->next;
					delete tmp2;
				}
			tmp = tmp->next;
		}
	}
}
//согласно теореме, доказанной Эйлером, эйлеров цикл существует тогда и только тогда, когда в графе отсутствуют вершины нечётной степени. 
int eiler(Node** gr, int num)
{
	int count;
	for (int i = 0; i < num; i++)
	{  
		count = 0;
		Node* tmp = gr[i];

		while (tmp)
		{      
			count++;
			tmp = tmp->next;
		}
		if (count % 2 == 1)
			return 0; // степень нечетная
	}
	return 1;   // все степени четные
}

void eiler_path(Node** gr)
{ //Построение цикла

	Node* S = NULL;
	int v = vertex;// 1я вершина (произвольная)
	int u;

	push(S, v); //сохраняем ее в стек
	while (S)
	{
		v = peek(S); // текущая вершина
		if (!gr[v]) 
		{                         // если нет инцидентных ребер
			v = pop(S); 
			std::cout << v + 1 << "   ";
		}
		else
		{
			u = gr[v]->inf; 
			push(S, u);  //проходим в следующую вершину
			del(gr[v], u); 
			del(gr[u], v); //удаляем пройденное ребро
		}
	}
}

int main()
{
	setlocale(LC_ALL, "RUS");
	int n;
	std::cout << "Количество вершин:  "; 
	std::cin >> n; // Количество вершин
	int present_n;// Текущее значение

	graph = new Node * [n];
	for (int i = 0; i < n; i++)
		graph[i] = NULL;
	for (int i = 0; i < n; i++)   // заполняем массив списков

		for (int j = 0; j < n; j++)
		{
			std::cin >> present_n;
			if (present_n) 
				add(graph[i], j);
		}

	if (eiler(graph, n))
	{
		std::cout << "Граф является эйлеровым" << std::endl;
		eiler_path(graph);
	}		
	else 
		std::cout << "Граф не является эйлеровым" << std::endl;
	return(0);
}