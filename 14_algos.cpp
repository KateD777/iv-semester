﻿#include <iostream>
#include <fstream>

#define NO_OF_CHARS 150

void Rabin(char pat[], char txt[], int q)
{
	int M = strlen(pat);
	int N = strlen(txt);
	int i, j;
	int P_hash = 0; 
	int T_hash = 0; 
	int h = 1;

	//хеш-функция строки (модулярная арифметика)
	for (i = 0; i < M - 1; i++)
		h = (h * NO_OF_CHARS) % q;

	for (i = 0; i < M; i++) 
	{
		P_hash = (NO_OF_CHARS * P_hash + pat[i]) % q;
		T_hash = (NO_OF_CHARS * T_hash + txt[i]) % q;
	}
	//перемещаем наш pat
	for (i = 0; i <= N - M; i++)
	{
		if (P_hash == T_hash) 
		{
			for (j = 0; j < M; j++) 
			{
				if (txt[i + j] != pat[j]) 
				{
					break;
				}
			}

			if (j == M)
				std::cout << pat << " найден под индексами: " << i << " - " << i + strlen(pat) - 1 << std::endl;
		}
		// узнаем хэш: удалич нач и добавим конеч
		if (i < N - M)
		{
			T_hash = (NO_OF_CHARS * (T_hash - txt[i] * h) + txt[i + M]) % q;

			if (T_hash < 0)  //на случай если t отриц
				T_hash = (T_hash + q);
		}
	}
}

int main()
{
	setlocale(LC_ALL, "RUS");
	int i = 0;
	char txt[100];
	char pat[10];
	std::cin >> pat;
	int q = INT_MAX; //чем больше значение q, тем ниже вероятность ложных срабатываний
	std::ifstream fin("input.txt");
	while (i != 100)
	{
		fin.get(txt[i]);
		i++;
	}
	Rabin(pat, txt, q);

	return 0;
}