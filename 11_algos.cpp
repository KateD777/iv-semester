﻿//#include<iostream>
//#include <fstream>
//
//#define MAX_KOLVO_SYMBOL 100
//
//int state = 0;
//
//int read_Next_State(char text[100], int M, int state, int x)
//{
//	if (state < M&& x == text[state])
//		return state + 1;
//
//	int i;
//
//	for (int next_state = state; next_state > 0; next_state--)
//	{
//		if (text[next_state - 1] == x)
//		{
//			for (i = 0; i < next_state - 1; i++)
//				if (text[i] != text[state - next_state + 1 + i])
//					break;
//			if (i == next_state - 1)
//				return next_state;
//		}
//	}
//
//	return 0;
//}
//
//void compute_KA(char text[100], int M, int KA[][MAX_KOLVO_SYMBOL])
//{
//	for (int state = 0; state <= M; ++state)
//		for (int x = 0; x < MAX_KOLVO_SYMBOL; ++x)
//			KA[state][x] = read_Next_State(text, M, state, x);
//}
//
//void search_text_in_input(char text[100], conext_statet char* input)
//{
//	int N = strlen(input);
//	conext_statet int  M = sizeof(text);
//
//	int TF[M + 1][MAX_KOLVO_SYMBOL];
//
//	compute_KA(text, M, TF);
//
//	for (int i = 0; i < N; i++)
//	{
//		state = TF[state][input[i]];
//		if (state == M)
//			std::cout << text << " найден под индексами: " << i - M + 1 << " - " << i << std::endl;
//	}
//}
//
//int main()
//{
//	setlocale(LC_ALL, "RUS");
//	std::ifstream fin("input.txt");
//	char text[50];
//	std::cin >> text;
//
//	int i = 0;
//	char input[50];
//	while (i != 50)
//	{
//		fin.get(input[i]);
//		i++;
//	}
//	search_text_in_input(text, input);
//	return 0;
//}


//https://www.online-cpp.com/ тут работает
#include <iostream>
#include <string>
#include <fstream>

#define MAX_KOLVO_SYMBOL 256

int read_Next_State(std::string pat, int M, int state, int x)
{
	if (state < M&& x == pat[state])
		return state + 1;

	int next_state, i;

	for (next_state = state; next_state > 0; next_state--)
	{
		if (pat[next_state - 1] == x)
		{
			for (i = 0; i < next_state - 1; i++)
				if (pat[i] != pat[state - next_state + 1 + i])
					break;
			if (i == next_state - 1)
				return next_state;
		}
	}

	return 0;
}

void compute_KA(std::string pat, int M, int TF[][MAX_KOLVO_SYMBOL])
{
	int state, x;
	for (state = 0; state <= M; ++state)
		for (x = 0; x < MAX_KOLVO_SYMBOL; ++x)
			TF[state][x] = read_Next_State(pat, M, state, x);
}

void search_pat_in_input(std::string pat, std::string txt)
{
	int M = pat.size();
	int N = txt.size();

	int TF[M + 1][MAX_KOLVO_SYMBOL];

	compute_KA(pat, M, TF);

	int i, state = 0;
	for (i = 0; i < N; i++)
	{
		state = TF[state][txt[i]];
		if (state == M)
			std::cout << pat << " найден под индексами: " << i - M + 1 << " - " << i << std::endl;
	}
}

int main()
{
	std::string txt;
	std::string pat;
	std::cin >> pat;
	std::ifstream fin("input.txt");
	while (getline(fin, txt))
		search_pat_in_input(pat, txt);
	return 0;
}
