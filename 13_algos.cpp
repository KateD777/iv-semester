﻿#include <iostream>
#include <string>
#include <fstream>
//сравнивает шаблон с текстом справа налево
#define NO_OF_CHARS 150
//правило плохого символа
void Bad_Heuristic(const char *str, int size, int badchar[NO_OF_CHARS])
{
    int i;

    for (int i = 0; i < NO_OF_CHARS; i++)  //все вхождения -1
        badchar[i] = -1;        
    for (int i = 0; i < size; i++) //заполним фактическое значение последнего вхождения символа
        badchar[(int)str[i]] = i;
}

void search(const char* txt, const char* pat) 
{
    int m = strlen(pat);
    int n = strlen(txt);

    int badchar[NO_OF_CHARS];//заполним массив неправильных символов с помощью bad_h

    Bad_Heuristic(pat, m, badchar);

    int s = 0;  //наш сдвиг шаблона относительно текста

    while (s <= (n - m)) 
    {
        int j = m - 1;

        while (j >= 0 && pat[j] == txt[s + j]) //Продолжим уменьшать j шаблона, пока символы шаблона и текста совпадают
            j--;

        if (j < 0) //Если шаблон присутствует в текущем сдвиге, тогда индекс j станет - 1 после цикла
        {
            std::cout << pat << " найден под индексами: " << s << " - " << s + strlen(pat) - 1 << std::endl;
           
            s += (s + m < n) ? m - badchar[txt[s + m]] : 1; //сдвигаем шаблон чтобы следующий символ в тексте совпадал с последним его появлением в шаблоне
                                                            //s+m < n необходимо когда шаблон встречается в конце текста 
        }
        else
            s += std::max(1, j - badchar[txt[s + j]]); //Сместите шаблон так, чтобы неправильный символ в тексте совпадал с последним его появлением в шаблоне 
                                                        //Функция max используется для того, чтобы убедиться, что мы получаем положительный сдвиг
    }
}

int main() 
{
    setlocale(LC_ALL, "RUS");
    std::ifstream fin("input.txt");
    char pat[100];
    std::cin >> pat;

    int i = 0;
    char txt[1000];
    while (i <= 100)
    {
        fin.get(txt[i]);
        i++;
    }

    search(txt, pat);
    return 0;
}