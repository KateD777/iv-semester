﻿//#include <iostream>
//#include <string>
//#include <fstream>
//
//void LPS(char pat[], int lps[], int M)  //lps [] указывает на самый длинный правильный префикс, который также является суффиксом
//{
//    int i = 1;
//    int len = 0;
//    lps[0] = 0;
//    while (i < M) 
//    {
//        if (pat[i] == pat[len]) 
//        {
//            len++;
//            lps[i] = len;
//            i++;
//        }
//        else 
//        {
//            if (len == 0) 
//            {
//                lps[i] = 0;
//                i++;
//            }
//            else 
//            {
//                len = lps[len - 1];
//            }
//        }
//    }
//}
//
//void KMP(char pat[], char s[]) 
//{
//    const int  M = sizeof(pat);
//    int N = strlen(s);
//    int i = 0;
//    int j = 0;
//    int lps[M];
//    LPS(pat, lps, M);          //lps [j-1] - количество символов pat [0 ... j-1], которые являются как правильным префиксом, так и суффиксом
//    while (i < N) 
//    {
//        if (s[i] == pat[j])    //нам не нужно сопоставлять эти lps [j-1] символы с s[ij ... i-1]
//        {
//            i++;
//            j++;
//            if (j == M)
//            {
//                std::cout << pat << " найден под индексами: " << i - j << " - " << i - 1 << std::endl;
//                j = lps[j - 1];
//            }
//        }
//        else 
//        {
//
//            if (j == 0) 
//                i = i + 1;
//            else 
//                j = lps[j - 1];
//        }
//    }
//
//}
//
//int main(int argc, const char* argv[]) 
//{
//    setlocale(LC_ALL, "Rus");
//    std::ifstream fin("input.txt");
//    int i = 0;
//    char s[100];
//    while (i <= 100)
//    {
//        fin.get(s[i]);
//        i++;
//    }
//
//    char pat[100];
//    std::cin >> pat;
//
//    KMP(pat, s);
//    return 0;
//}

#include <iostream>
#include <fstream>
#include <string>

void LPS(std::string pat, int m, int lps[])
{
    int l = 0;

    lps[0] = 0;

    int i = 1;
    while (i < m) 
    {
        if (pat[i] == pat[l]) 
        {
            l++;
            lps[i] = l;
            i++;
        }
        else
        {
            if (l != 0) 
            {
                l = lps[l - 1];
            }
            else
            {
                lps[i] = 0;
                i++;
            }
        }
    }
}

void KMP(std::string pat, std::string str)
{
    int m = pat.length();
    int n = str.length();

    int lps[m];

    LPS(pat, m, lps);

    int i = 0;
    int j = 0;
    while (i < n) {
        if (pat[j] == str[i]) 
        {
            j++;
            i++;
        }

        if (j == m) 
        {
            std::cout << pat << " найден под индексами: " << i - j << std::endl;
            j = lps[j - 1];
        }

        else if (i < n && pat[j] != str[i]) 
        {
            if (j != 0)
                j = lps[j - 1];
            else
                i = i + 1;
        }
    }
}

int main()
{
    std::string str;
    std::string pat;
    std::cin >> pat;
    std::ifstream fin("input.txt");
    while (getline(fin, str))
        KMP(pat, str);
    return 0;
}