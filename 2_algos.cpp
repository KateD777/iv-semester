﻿#include <iostream>
#include <string>
#include <list>
#include <iterator>
#include <vector>
#include <queue>
#include <fstream>
const int n = 8;

class Graph
{
public:
	Graph(int count)
	{
		Num = count;
		color = new std::string[count];
		d = new int[count];
		v = new std::list<int>[count];
	}
	void Add()
	{
		int element;
		std::ifstream in; 
		in.open("input.txt");
		for (int i = 0; i < n; i++)
			for (int j = 0; j < n; j++)
			{
				in >> element;
				if (element == 1)
					v[i].push_back(j);
			}
		in.close();
	}
	void BFS(int s)
	{
		std::queue<int> Q;
		for (int i = 0; i < Num; i++)
		{
			color[i] = "w";
			d[i] = INT_MAX;
		}
		d[s] = 0;
		Q.push(s);
		int u = 0;
		while (!Q.empty())
		{
			u = Q.front(); Q.pop();
			for (auto i = v[u].begin(); i != v[u].end(); i++)
				if (color[*i] == "w")
				{
					color[*i] = "g";
					d[*i] = d[u] + 1;
					Q.push(*i);
				}
			color[u] = "b";
		}


		std::ofstream out; 
		out.open("output.txt");
		out << "Путь от " << s << std::endl;
		for (int i = 0; i < Num; i++)
			out << i << ": " << d[i] << std::endl;

		out.close();
	}


private:
	int Num;
	std::string* color;
	std::list<int>* v;
	int* d;
};

int main()
{
	Graph G(n);
	G.Add();
	G.BFS(0);
	return 0;
}
