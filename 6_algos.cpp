﻿#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

#define edge pair<int,int>

class Graph 
{
private:
    vector<pair<int, edge>> G; // граф
    vector<pair<int, edge>> T; // остовное
    int* parent;
    int V; // число ребер между вершинами
public:
    Graph(int V);
    void AddWeightedEdge(int u, int v, int w);
    int find_set(int i);
    void edge_of_vertices(int u, int v);
    void kruskal();
    void print();
};

Graph::Graph(int V) 
{
    parent = new int[V];

    for (int i = 0; i < V; i++)
        parent[i] = i;

    G.clear();
    T.clear();
}

void Graph::AddWeightedEdge(int u, int v, int w) 
{
    G.push_back(make_pair(w, edge(u, v)));
}

int Graph::find_set(int i)
{
    if (i == parent[i])
        return i;
    else
        // Иначе мы рекурсивно вызываем Find для его родительского элемента
        return find_set(parent[i]);
}

void Graph::edge_of_vertices(int u, int v) 
{
    parent[u] = parent[v];
}

void Graph::kruskal() 
{
    int U, V;
    sort(G.begin(), G.end()); // рёбра сортируются по весу
    for (int i = 0; i < G.size(); i++) 
    {
        U = find_set(G[i].second.first);
        V = find_set(G[i].second.second);
        if (U != V) //если концы ребра принадлежат разным поддеревьям, то эти поддеревья объединяются, а ребро добавляется к ответу
        {
            T.push_back(G[i]);
            edge_of_vertices(U, V);
        }
    }
}
void Graph::print() 
{
    cout << "Ребро и его вес" << endl;
    for (int i = 0; i < T.size(); i++) 
        cout << T[i].second.first << "-" << T[i].second.second << " = " << T[i].first << endl;
}
int main() 
{
    setlocale(LC_ALL, "RUS");
    Graph g(10);

    g.AddWeightedEdge(0, 1, 4);
    g.AddWeightedEdge(1, 0, 4);

    g.AddWeightedEdge(0, 2, 3);
    g.AddWeightedEdge(2, 0, 3);

    g.AddWeightedEdge(1, 2, 2);
    g.AddWeightedEdge(2, 1, 2);

    g.AddWeightedEdge(3, 2, 3);
    g.AddWeightedEdge(2, 3, 3);

    g.AddWeightedEdge(5, 2, 2);
    g.AddWeightedEdge(2, 5, 2);

    g.AddWeightedEdge(2, 4, 4);
    g.AddWeightedEdge(4, 2, 4);

    g.AddWeightedEdge(3, 4, 3);
    g.AddWeightedEdge(4, 3, 3);

    g.AddWeightedEdge(1, 4, 7);
    g.AddWeightedEdge(4, 1, 7);

    g.AddWeightedEdge(3, 5, 8);
    g.AddWeightedEdge(5, 3, 8);

    g.kruskal();
    g.print();

    return 0;
}