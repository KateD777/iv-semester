﻿//алгоритм косарайю
#include<iostream>
#include<vector>
#include<stack>
#include <fstream>

std::vector<int> edges1[100];

std::vector<int> edges2[100];
int used1[100];
int used2[100];
int number_edges;
int number_vertices;
std::stack<int> st;

void read_graph()
{
    int v, u;

    std::ifstream fin1("input1.txt");
    fin1 >> number_edges >> number_vertices;
    while (number_vertices--)
    {
        fin1 >> v >> u;
        edges1[v].push_back(u);
        edges2[u].push_back(v);
    }
}

void DFS(int node) 
{
    used1[node] = 1;
    for (int child : edges1[node]) 
    {
        if (used1[child] != 1) 
        {
            DFS(child);
        }
    }
    st.push(node);
}

//DFS обход модифицированного графа
void DFS2(int v) 
{
    std::cout << v << " ";
    used2[v] = 1;
    for (int child : edges2[v]) 
    {
        if (used2[child] != 1) 
        {
            DFS2(child);
        }
    }
}
int main() 
{
    setlocale(LC_ALL, "RUS");
    read_graph();
    DFS(0);
    int kolvo = 0;
    std::cout << "Cильно связанные компоненты: ";
    while (!st.empty()) 
    {
        int k = st.top();
        st.pop();
        if (used2[k] != 1) 
        {
            DFS2(k);
            if (k % 2 != 1)
                kolvo++;
            std::cout << std::endl;
        }
    }
    std::cout << std::endl;
    std::cout << "Количество компонентов: " << kolvo << std::endl;

    return 0;
}