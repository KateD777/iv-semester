﻿#include <iostream>
#include <fstream>

void Dijkstra(int **G, int V, int start)
{
	int* distance = new int [V];
	bool* used = new bool[V];
	int index;

	for (int i = 0; i < V; i++)
	{
		distance[i] = INT_MAX; 
		used[i] = false;
	}

	distance[start] = 0;

	for (int count = 0; count < V - 1; count++)
	{
		int min = INT_MAX;
		for (int i = 0; i < V; i++)
		{
			if (!used[i] and distance[i] <= min)
			{
				min = distance[i];
				index = i;
			}
		}
	
		used[index] = true;
		for (int j = 0; j < V; j++)
			if (!used[j] and G[index][j] and distance[index] != INT_MAX and distance[index] + G[index][j] < distance[j])
				distance[j] = distance[index] + G[index][j];

	}

	std::fstream out;
	out.open("output1.txt");      // открываем файл для записи
	if (out.is_open())
	{
		out << "Кратчайший путь из " << start+1 << " вершины до остальных: \t\n";
		for (int i = 0; i < V; i++)
		{
			if (distance[i] != INT_MAX)
				out << start + 1 << " - " << i + 1 << " = " << distance[i] << std::endl;
		}
	}
	out.close();
	
}

int main()
{
	int V = 0;
	setlocale(LC_ALL, "RUS");
	std::ifstream fin("input1.txt");	
	fin >> V;
	int** G = new int* [V];

	for (int i = 0; i < V; i++) 
		G[i] = new int [V];

	for (int i = 0; i < V; i++)
		for (int j = 0; j < V; j++)
			fin >> G[i][j];

	Dijkstra(G, V, 0);

}